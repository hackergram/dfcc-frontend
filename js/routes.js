app.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "views/home.html",
        controller: 'homeController'
    })
    .when("/about", {
        templateUrl : "views/about.html",
        controller: 'aboutController'
    })
    .when("/team", {
        templateUrl : "views/team.html",
        controller: 'teamController'
    })
    .when("/dashboard", {
        templateUrl : "views/dashboard.html",
        controller: 'dashboardController'
    })

});
