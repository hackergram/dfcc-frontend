app.controller("dashboardController", function(
  $window,
  Upload,
  $cookieStore,
  vcRecaptchaService,
  $scope,
  $http,
  $location,
  $rootScope
) {
  if (
    $cookieStore.get("id") == undefined ||
    $cookieStore.get("token") == undefined ||
    $cookieStore.get("id") == "" ||
    $cookieStore.get("token") == ""
  ) {
    $location.path("/");
  }

  $scope.cases = [];
  $scope.caseimage = null;
  $scope.createCaseProgress = 0;
  $scope.creatingcase = false;


  $scope.progressStyle = {
    width: $scope.createCaseProgress + "%",
    display: "none"
  };
  $scope.hideSize = {
    display: "none"
  };
  $scope.hideFile = {
    display: "none"
  };
  $scope.loading = {
    display: "flex"
  };
  $scope.btnName = "Create";
  $scope.toggle = "collapse";

  $scope.errorText = "";

  $scope.logout = function() {
    var formData = {
      token: $cookieStore.get("token"),
      id: $cookieStore.get("id"),
      action: "logout"
    };

    Upload.upload({
      url: API_END_POINT,
      data: formData,
      method: "POST"
    }).then(
      function(response) {
        console.log(response);
        if (response.data.code == 200) {
          $cookieStore.remove("id");
          $cookieStore.remove("token");
          $location.path("/");
        }
      },
      function(response) {
        console.log(response);
      },
      function(evt) {
        console.log(Math.min(100, parseInt((100.0 * evt.loaded) / evt.total)));
      }
    );
  };



  $scope.createcase = function() {

    $scope.hideFile = {
        display: "none"
    };
    
    $scope.creatingcase = true;
    var formData = {
      token: $cookieStore.get("token"),
      userid: $cookieStore.get("id"),
      message: $scope.message,
      action: "insertCase",
      file: $scope.caseimage
    };

    Upload.upload({
      url: API_END_POINT,
      data: formData,
      method: "POST"
    }).then(
      function(response) {
        if (response.data.code == 200) {
          $scope.cases.push(response.data.data);
        }
        else{
            $scope.hideFile = {
                display: "block"
            };
            $scope.errorText = response.data.message;
        }
        $scope.creatingcase = false;
      },
      function(response) {
        console.log(response);
        $scope.creatingcase = false;

      },
      function(evt) {
        // console.log(Math.min(100, parseInt((100.0 * evt.loaded) / evt.total)));
        $scope.createCaseProgress = Math.min(
          100,
          parseInt((100.0 * evt.loaded) / evt.total)
        );
        $scope.progressStyle = {
          width: $scope.createCaseProgress + "%",
          display: "inline-block"
        };
      }
    );
  };

  $scope.listcases = function() {
    
    var formData = {
      token: $cookieStore.get("token"),
      userid: $cookieStore.get("id"),
      action: "listCases"
    };

    Upload.upload({
      url: API_END_POINT,
      data: formData,
      method: "POST"
    }).then(
      function(response) {
        //console.log(response);
        if (response.data.status == 200) {
          $scope.cases = response.data.data;
        }
        $scope.loading = {
            display: "none"
        };
      },
      function(response) {
        $scope.loading = {
            display: "none"
        };
        console.log(response);
      },
      function(evt) {
        console.log(Math.min(100, parseInt((100.0 * evt.loaded) / evt.total)));
      }
    );
  };

  $scope.listcases();

  $scope.error = {};

  $scope.$watch("file", function(data) {
    if (data) {
      var filename = data.name;
      if (
        filename.split(".").pop() != "JPG" &&
        filename.split(".").pop() != "jpg" &&
        filename.split(".").pop() != "jpeg" &&
        filename.split(".").pop() != "JPEG" &&
        filename.split(".").pop() != "PNG" &&
        filename.split(".").pop() != "png"
      ) {
        $scope.errorText = "Only image(jpg/png) files are supported";
        $scope.hideFile = {
          display: "block"
        };
      } else if (data.size >= 8192000) {
        $scope.errorText = "File should be less than 8 mb";
        $scope.hideFile = {
          display: "block"
        };
      } else {
        $scope.hideFile = {
          display: "none"
        };
        $scope.fileNameError = false;
        $scope.sizeError = false;
        $scope.caseimage = data;
      }
    }
  });
});
