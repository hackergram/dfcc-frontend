app.controller('homeController', function($window,Upload,$cookieStore,vcRecaptchaService,$scope,$http,$location,$rootScope) {
  
  if ( $cookieStore.get('id')!=undefined && $cookieStore.get('token') != undefined && $cookieStore.get('id') != '' && $cookieStore.get('token') != '' ){
    $location.path('/dashboard');
  }


  /*
    $scope.model = { key: '6Lcz0WoUAAAAAKqSgYn2pV6D_qwLJ6f04Z8X8o46' };

    if ( $cookieStore.get('cookie') == 1 ){
        $scope.cookieShow = false;
    }
    else{
        $cookieStore.put('cookie', '1');
        $scope.cookieShow = true;
    }

    $scope.hideCookiepopup = function(){
        $scope.cookieShow = false;
    }


      $scope.captcharesponse = null;
      $scope.widgetId        = null;
      $scope.error           = '';
      $scope.requested       = false;
      $scope.registerationsuccess = false;
      $scope.passwordtxt        = '';
    
      $scope.setResponse = function (response) {
        $scope.captcharesponse = response;
        $scope.status = ''; // Clear status 
      };

      $scope.setWidgetId = function (widgetId) {
        $scope.widgetId = widgetId;
      };

      $scope.cbExpiration = function() {
        vcRecaptchaService.reload($scope.widgetId);
        $scope.captcharesponse = null;
      };

      $scope.register = function(){
          $scope.error     = '';

          if ( !isValidPassword( $scope.passwordtxt ) ){
            $scope.error = 'Invalid Password';
            var pElement = $window.document.getElementById('password');
            pElement.focus();
          }
          else{
            $scope.requested = true;
            $scope.registerationsuccess = false;
            var formData = {
              fname   : $scope.fname,
              lname   : $scope.lname,
              industry: $scope.industryname,
              company : $scope.companyname,
              email   : $scope.email,
              password: $scope.passwordtxt,
              captcha : $scope.captcharesponse,
              timezone: $scope.timezone
            };
  
            Upload.upload({
              url: API_END_POINT+'/register',
              data: formData,
              method: 'POST'
            }).then(function (response) {
              console.log (response);
              $scope.registerationsuccess = true;
              $scope.requested = false;
              $scope.cbExpiration();
            }, function (response) {
              console.log (response);
              $scope.registerationsuccess = false;
              $scope.requested            = false;
              $scope.error                = response.data.message;
              $scope.cbExpiration();
            }, function (evt) {
              console.log(Math.min(100, parseInt(100.0 * evt.loaded / evt.total)));
            });
          }
      }

      $scope.validatePassword =function( property ){
        
        if ( $scope.passwordtxt == undefined ){
          return false;
        }
        else if( property == 'length' ){
          return ( $scope.passwordtxt.length > 6 ) ? true : false;
        }
        else if ( property == 'capital' ){
          return (/[A-Z]/.test($scope.passwordtxt));
        }
        else if ( property == 'lowercase' ){
          return (/[a-z]/.test($scope.passwordtxt));
        }
        else if ( property == 'number' ){
          return (/[0-9]/.test($scope.passwordtxt));
        }
      }

      var isValidPassword = function( password ){
        return ( password.length > 6 && /[A-Z]/.test(password) && /[a-z]/.test(password) && /[0-9]/.test(password) );
      }*/
      $scope.requested       = false;
      $scope.validcontact    = false;

      $scope.user_error      = '';
      $scope.user_requested  = '';
      $scope.captcharesponse = null;
      $scope.widgetId        = null;

      $scope.model = { key: '6Lcz0WoUAAAAAKqSgYn2pV6D_qwLJ6f04Z8X8o46' };

      
      $scope.setResponse = function (response) {
        $scope.captcharesponse = response;
        $scope.status = ''; // Clear status 
      };

      $scope.setWidgetId = function (widgetId) {
        $scope.widgetId = widgetId;
      };

      $scope.cbExpiration = function() {
        vcRecaptchaService.reload($scope.widgetId);
        $scope.captcharesponse = null;
      };

    

      $scope.checkUser = function(){
        $scope.error     = '';

        if ( $scope.contact == undefined || ( $scope.contact == '' || $scope.contact.length != 10 ) ){
          $scope.error = 'Invalid Contact';
        }
        else{
          $scope.requested = true;
          
          var formData = {
            contact   : $scope.contact,
            action    : 'checkpatient'
          };

          Upload.upload({
            url: API_END_POINT,
            data: formData,
            method: 'POST'
          }).then(function (response) {
            $scope.requested = false;
            if ( response.data.code == 200 ){
              $scope.validcontact = true;
            }
            else{
              $('#exampleModal').modal("show");
              $scope.user_contact = $scope.contact;
              $scope.contact = '';
            }
          }, function (response) {
            $scope.requested = false;
            $scope.error     = 'Try Again';
          }, function (evt) {
            console.log(Math.min(100, parseInt(100.0 * evt.loaded / evt.total)));
          });
        }
    }

    $scope.loginUser = function(){
      $scope.error     = '';

      if ( $scope.password == undefined || $scope.password == '' ){
        $scope.error = 'Invalid Password';
      }
      else{
        $scope.requested = true;
        
        var formData = {
          password  : $scope.password,
          contact   : $scope.contact,
          type      : 0,
          action    : 'login'
        };

        Upload.upload({
          url: API_END_POINT,
          data: formData,
          method: 'POST'
        }).then(function (response) {
          $scope.requested = false;
          if ( response.data.code == 200 ){
            $cookieStore.put('id', response.data.id );
            $cookieStore.put('token', response.data.token );
            $location.path('/dashboard');
          }
          else{
            $scope.error     = '';
          }
        }, function (response) {
          $scope.requested = false;
          $scope.error     = 'Try Again';
        }, function (evt) {
          console.log(Math.min(100, parseInt(100.0 * evt.loaded / evt.total)));
        });
      }
  }


  $scope.register = function(){
    $scope.user_error     = '';

    if ( $scope.user_name != '' && $scope.user_pincode != '' && $scope.user_contact != '' && $scope.captcharesponse != null ){
      $scope.user_requested = true;
      
      var formData = {
        name     : $scope.user_name,
        pincode  : $scope.user_pincode,
        contact  : $scope.user_contact,
        action   : 'insertPatient',
        captcha  : $scope.captcharesponse
      };

      Upload.upload({
        url: API_END_POINT,
        data: formData,
        method: 'POST'
      }).then(function (response) {
        $scope.user_requested = false;
        $scope.cbExpiration();
        if ( response.data.code == 200 ){
          $scope.user_error = '';
          $cookieStore.put('id', response.data.id );
          $cookieStore.put('token', response.data.token );
          $('#exampleModal').modal("hide");
          $location.path('/dashboard');
        }
        else{
          $scope.user_error  = response.data.message;
        }
      }, function (response) {
        $scope.user_requested = false;
        $scope.cbExpiration();
        $scope.user_error     = 'Try Again';
      }, function (evt) {
        console.log(Math.min(100, parseInt(100.0 * evt.loaded / evt.total)));
      });
    }
    else{
      $scope.user_error = 'Enter All fields';
    }
}

});

	
